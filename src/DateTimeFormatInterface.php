<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-model-datetime-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DateTimeFormat;

/**
 * DateTimeFormatInterface class file.
 * 
 * This class represent date and time formats. Date and time formats and
 * represents vary largely across different Information Systems, and this
 * interface is to make the links between them possible.
 * 
 * @author Anastaszor
 */
interface DateTimeFormatInterface
{
	
	/**
	 * Gets whether this date and time storage format uses dates internaly.
	 * 
	 * @return boolean
	 */
	public function usesDate() : bool;
	
	/**
	 * Gets whether this date and time storage format uses times internally.
	 * 
	 * @return boolean
	 */
	public function usesTime() : bool;
	
	/**
	 * Gets whether this date and time storage format uses subdivisions of
	 * seconds internally.
	 * 
	 * @return boolean
	 */
	public function usesMicrotime() : bool;
	
	/**
	 * Gets whether this date and time storage format uses timestamps internally.
	 * 
	 * @return boolean
	 */
	public function usesTimestamp() : bool;
	
	/**
	 * Gets whether this date and time storage format uses timezones internally.
	 * 
	 * @return boolean
	 */
	public function usesTimezone() : bool;
	
	/**
	 * Gets the earlyest date and time value available with this storage format,
	 * formatted with \DateTime::RFC3339_EXTENDED format.
	 * 
	 * @return string
	 */
	public function getEarlyestDateTime() : string;
	
	/**
	 * Gets the latest date and time value available with this storage format,
	 * formatted with \DateTime::RFC3339_EXTENDED format.
	 * 
	 * @return string
	 */
	public function getLatestDateTime() : string;
	
	/**
	 * Gets the string that should be used for php's native date() function or
	 * php's native \DateTime::createFromFormat() method.
	 * 
	 * @return string
	 */
	public function getPhpDateTimeFormat() : string;
	
	/**
	 * Gets whether two date and time formats are equals.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Gets the date and time format that is able to represent date and times
	 * for the ranges of both this date and time format and the other date and
	 * time format.
	 * 
	 * @param DateTimeFormatInterface $other
	 * @return DateTimeFormatInterface
	 */
	public function mergeWith(DateTimeFormatInterface $other) : DateTimeFormatInterface;
	
}
